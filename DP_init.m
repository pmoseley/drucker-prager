%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Original Author: Philip Moseley
%%                  Northwestern University
%%                  Philip.Moseley@u.northwestern.edu
%%  This code is licensed under the AFL v3.0 as freely distributable
%%  open source software. The full text of the AFL and all licensing
%%  information must be included with the distribution of this code.
%%
%% Upstream contributions are encouraged!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Function to initialize variables.
function [matl,sigma,F,kappa,Q] = DP_init()
I = [1 1 1 0 0 0]';     % Voigt rank2 equivalent identity tensor.
matl.nu = 0.30;         % Poisson's Ratio.
matl.E = 25000;         % Young's Modulus (kPa).
matl.Et = 0.1*matl.E;   % Tangential Modulus.
matl.alpha0 = 0.7;      % Material constants.
matl.beta0 = 0.7;
matl.a = 0.25;
matl.k = 0.1;
sigma = -50*I;          % Initial stresses.


% Derived material constants.
matl.K = matl.E/(3*(1-2*matl.nu));        % Bulk Modulus.
matl.mu = matl.E/(2*(1+matl.nu));         % Lame's constants.
matl.lambda = matl.E*matl.nu/((1+matl.nu)*(1-2*matl.nu));
% 2nd order tensor of elastic constants. Isotropic linear elastic.
matl.Ce = 2*matl.mu*eye(6) + matl.lambda*(I*I');
% Elasticity matrix in principal axes (for spectrally decomposed eqns).
matl.ae = (matl.K+4*matl.mu/3)*eye(3) + (matl.K-2*matl.mu/3)*(~eye(3));

% Vector of plastic internal variables (PIVs).
kappa.k = @(lambda) [matl.alpha0 + 2*matl.a*sqrt(matl.k*lambda)/(matl.k+lambda);
                     matl.alpha0 + 2*matl.a*sqrt(matl.k*lambda)/(matl.k+lambda)...
                     - matl.beta0];
% h = dkappa/dlambda
kappa.H = @(sigma,lambda) [1;1] .*...
    matl.a*matl.k*(matl.k-lambda)/((matl.k+lambda)^2*sqrt(matl.k*lambda));
kappa.dHdsig = @(sigma,lambda) [0 0 0; 0 0 0];
kappa.dHdlam = @(sigma,lambda) [1;1] .*...
    -matl.a*matl.k^2*(matl.k^2+6*matl.k*lambda-3*lambda^2)/...
    (2*(matl.k*lambda)^(1.5)*(matl.k+lambda)^3);

% Yield surface.
F.F = @(sigma,kappa) sqrt(3/2)*norm(sigma-sum(sigma)/3) + (kappa(1)/3)*sum(sigma);
F.dFdsig = @(sigma,kappa) kappa(1)/3 + 1/2.*...
    [(2*sigma(1) - sigma(2) - sigma(3));
    -(sigma(1) - 2*sigma(2) + sigma(3));
    -(sigma(1) + sigma(2) - 2*sigma(3));]'...
    ./sqrt(sigma(1)^2+sigma(2)^2-sigma(2)*sigma(3)+sigma(3)^2-sigma(1)*(sigma(2)+sigma(3)));
F.dFdk = @(sigma,kappa) [(sigma(1)+sigma(2)+sigma(3))/3, 0.0];

% Plastic potential Q.
Q.dQdsig = @(sigma,kappa) kappa(2)/3 + 1/2.*...
    [(2*sigma(1) - sigma(2) - sigma(3));
    -(sigma(1) - 2*sigma(2) + sigma(3));
    -(sigma(1)  +sigma(2) - 2*sigma(3));]...
    ./sqrt(sigma(1)^2+sigma(2)^2-sigma(2)*sigma(3)+sigma(3)^2-sigma(1)*(sigma(2)+sigma(3)));
Q.d2Qdsig2 = @(sigma,kappa) 3/4.*...
    [(sigma(2)-sigma(3))^2,  (sigma(1)-sigma(3))*(sigma(3)-sigma(2)),...
        (sigma(1)-sigma(2))*(sigma(2)-sigma(3));
     (sigma(1)-sigma(3))*(sigma(3)-sigma(2)),  (sigma(1)-sigma(3))^2,...
        -(sigma(1)-sigma(2))*(sigma(1)-sigma(3));
     (sigma(1)-sigma(2))*(sigma(2)-sigma(3)),  -(sigma(1)-sigma(2))*(sigma(1)-sigma(3)),...
        (sigma(1)-sigma(2))^2;]...
    ./(sigma(1)^2+sigma(2)^2-sigma(2)*sigma(3)+sigma(3)^2-sigma(1)*(sigma(2)+sigma(3)))^(3/2);
Q.d2Qdsigdk = @(sigma,kappa) [0 1; 0 1; 0 1]./3;

return
