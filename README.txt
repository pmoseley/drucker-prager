--------------AUTHOR-----------------------------------
Philip Moseley
PhD Student, Belytschko Research Group
Northwestern University
Philip.Moseley@u.northwestern.edu

--------------LICENSE----------------------------------
This code is licensed under the Academic Free License v3.0. You may modify and distribute the code as you please, as long as you maintain the attribution notices.

--------------USAGE------------------------------------
Material constants must be defined in DP_init.m
Loading conditions are defined in main.m
Run main.m

--------------REFERENCES-------------------------------
R.I. Borja and J.E. Andrade. Critical state plasticity. part vi: Meso-scale finite element simulation of strain localization in discrete granular materials. Computer Methods in Applied Mechanics and Engineering, 195(37-40):5115–5140, 2006.

R.I. Borja, K.M. Sama, and P.F. Sanz. On the numerical integration of three-invariant elastoplastic constitutive models. Computer Methods in Applied Mechanics and Engineering, 192(910):1227–1258, 2003.

