%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Original Author: Philip Moseley
%%                  Northwestern University
%%                  Philip.Moseley@u.northwestern.edu
%%  This code is licensed under the AFL v3.0 as freely distributable
%%  open source software. The full text of the AFL and all licensing
%%  information must be included with the distribution of this code.
%%
%% Upstream contributions are encouraged!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Integration function.
function [sigma,kappa_n1,lambda_n1,CTO] = integrator(matl,F,kappa,Q,sigma_tr,kappa_n,lambda_n)
I = [1 1 1 0 0 0]';         % Voigt rank2 equivalent identity tensor.
TOL = 10e-10;

% Calculate trial stresses in spectral domain (principal axes, 3x1).
% Eigenvectors for strain are the same as the eigenvectors for sigma and sigma_tr.
epsilon_e = matl.Ce\sigma_tr;
[eig_vecs, spectral_dirs, epsilon_spec] = voigt_eig(epsilon_e);
sigma_tr_spec = matl.ae*epsilon_spec;

%================ Check if elastic ==================
if(F.F(sigma_tr_spec,kappa_n) <= 0)    % Yield Surface F = 0
    CTO = matl.Ce;
    kappa_n1 = kappa_n;
    lambda_n1 = lambda_n;
    sigma = sigma_tr;
    return;
end

%================ Inelastic Material Response ==================
% Use Newton-Raphson iterations to solve all the equations.
d_lambda = 0.00001;     %TODO: prevents divide by zero problems in kappa.H and kappa.dHdsig
lambda_n1 = lambda_n + d_lambda;
kappa_n1 = kappa_n;
x = [sigma_tr_spec; kappa_n1; d_lambda];
r = local_residual(F,kappa,Q,matl.ae,x,lambda_n1,epsilon_spec,kappa_n);
limit = norm(r);
for i=1:50
    A = local_Jacobian(F,kappa,Q,matl.ae,x,lambda_n1);
    x = x - A\r;
    [sigma_tr_spec, kappa_n1, d_lambda] = NR_split(x);
    lambda_n1 = lambda_n + d_lambda;
    r = local_residual(F,kappa,Q,matl.ae,x,lambda_n1,epsilon_spec,kappa_n);
    fprintf('norm(r): %g, i:%i\n',norm(r)/limit,i);
    if(norm(r)/limit <= TOL) break; end
end

% Calculate the consistent tangent operator.
% CTO = matl.Ce;   % Testing. Should converge, but slowly.
% Get A for the final, converged values of x.
Ainv = inv(local_Jacobian(F,kappa,Q,matl.ae,x,lambda_n1));
CTO = zeros(6);
for a=1:3
    for b=1:3
        CTO = CTO + Ainv(a,b)*spectral_dirs(:,a)*spectral_dirs(:,b)';
        if(a ~= b)
            m_ab = voigtify(eig_vecs(:,a)*eig_vecs(:,b)');
            m_ba = voigtify(eig_vecs(:,b)*eig_vecs(:,a)');
            if(epsilon_spec(b)==epsilon_spec(a))
                disp('damn it');
            else
                CTO = 0.5*(sigma_tr_spec(b)-sigma_tr_spec(a))/(epsilon_spec(b)-epsilon_spec(a))...
                      * m_ab*(m_ab+m_ba)' + CTO;
            end
        end
    end
end
if(CTO(4,4) == 0.0) CTO(4,4) = 1.0; end  %TODO: hack to avoid singular matrices.

% Calculate final stresses in regular axes (6x1).
sigma = reverse_spectral(sigma_tr_spec, spectral_dirs);
end


%==================== Helper Functions =========================
% Convert a voigt tensor to a full tensor.
function A_full = de_voigtify(A_voigt)
    A_full = [A_voigt([1,6,5])';A_voigt([6,2,4])';A_voigt([5,4,3])'];
end

% Convert a full tensor to a voigt tensor.
function A_voigt = voigtify(A_full)
    A_voigt = A_full([1,5,9,6,3,2])';
end

% Calculate the eigenvalues and spectral directions for voigt notation tensors.
% Eigenvectors are 3x1 columns.
% Spectral directions are 6x1 columns, voigt notation.
function [eig_vectors, spectral_dirs, spectral_val] = voigt_eig(voigt_tensor)
    [eig_vectors, spectral_val] = eig(de_voigtify(voigt_tensor));
    spectral_val = spectral_val([1,5,9])';
    spectral_dirs = zeros(6,3);
    for i=1:3
        spectral_dirs(:,i) = voigtify(eig_vectors(:,i)*eig_vectors(:,i)');
    end
end

% Calculate spectral values in the regular axes.
function voigt = reverse_spectral(spectral_val, spectral_dir)
    voigt = zeros(6,1);
    for i=1:3
        voigt = voigt + spectral_val(i)*spectral_dir(:,i);
    end
end

% Calculate the local Jacobian matrix.
function A = local_Jacobian(F,kappa,Q,ae,x,lambda)
    [sigma, kappa_n1, d_lambda] = NR_split(x);
    n_piv = length(kappa_n1);
    A = zeros(4+n_piv);
    % First rows.
    A(1:3,1:3)       = inv(ae) + d_lambda*Q.d2Qdsig2(sigma,lambda);
    A(1:3,4:end-1)   = d_lambda*Q.d2Qdsigdk(sigma,lambda);
    A(1:3,end)       = Q.dQdsig(sigma,kappa_n1);
    % Second rows.
    A(4:end-1,1:3)   = -d_lambda*kappa.dHdsig(sigma,lambda);
    A(4:end-1,4:end-1) = eye(n_piv,n_piv);
    A(4:end-1,end)   = -kappa.H(sigma,lambda)-d_lambda*kappa.dHdlam(sigma,lambda);
    % Third rows.
    A(end,1:3)       = F.dFdsig(sigma,kappa_n1);
    A(end,4:end-1)   = F.dFdk(sigma,kappa_n1);
    A(end,end)       = 0.0;
end

% Calculate the local residual.
function r = local_residual(F,kappa,Q,ae,x,lambda,epsilon_spec,kappa_n)
    [sigma, kappa_n1, d_lambda] = NR_split(x);
    r = [ae\sigma - epsilon_spec + d_lambda*Q.dQdsig(sigma,kappa_n1);
         % kappa_n1 - kappa_n - d_lambda*kappa.H(sigma,lambda);     % Use this if we don't have kappa.k
         kappa_n1 - kappa.k(lambda);
         F.F(sigma,kappa_n1)];
end

% Split up x into component variables.
function [sigma_tr_spec, kappa_n1, d_lambda] = NR_split(x)
    sigma_tr_spec = x(1:3);
    kappa_n1 = x(4:end-1);
    d_lambda  = x(end);
end

