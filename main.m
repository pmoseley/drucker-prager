%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Original Author: Philip Moseley
%%                  Northwestern University
%%                  Philip.Moseley@u.northwestern.edu
%%  This code is licensed under the AFL v3.0 as freely distributable
%%  open source software. The full text of the AFL and all licensing
%%  information must be included with the distribution of this code.
%%
%% Upstream contributions are encouraged!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function main
%===================== Pre-Processing ======================%
[matl,sigma,F,kappa,Q] = DP_init();
% [matl,sigma,F,kappa,Q] = DP_2invariant_init();
TOL = 10e-10;    % Iteration tolerance.

t = 1:1:16;
%% Axisymmetric Loading %%
% strain_ctrl_idxs = 1;
% d_strain = @(t) -0.002*[1 0 0 0 0 0];   % Vertical strain control.
% stress = @(t) [0 -50 -50 0 0 0];        % Horizontal (radial) stress control.
%% Plane-Strain Loading %%
strain_ctrl_idxs = 1:2;
d_strain = @(t) -0.002*[1 0 0 0 0 0];   % Vertical strain control, y-axis plane strain control.
stress = @(t) [0 0 -50 0 0 0];          % Horizontal stress control.


%======================== Solution =========================%
% Initialize arrays.
I = [1 1 1 0 0 0]';     % Voigt rank2 equivalent identity tensor.
stresses = zeros(length(t),6); strains = zeros(length(t),6);
epsilon_s = zeros(length(t),1); epsilon_v = zeros(length(t),1);
q = zeros(length(t),1);

% Set initial conditions.
lambda_n = 0.0;                     % Cumulative plastic multiplier.
kappa_n = kappa.k(lambda_n);        % Vector of plastic internal variables (PIVs).
stresses(1,:) = sigma';             % Initial stress.
strains(1,:) = (matl.Ce\sigma)';    % Initial strain.
q(1) = sqrt(3/2)*voigt_norm(sigma - sum(sigma(1:3))/3.0*I);
epsilon_s(1) = sqrt(2/3)*voigt_norm(strains(1,:)'-sum(strains(1,1:3))/3*I);
epsilon_v(1) = sum(strains(1,1:3));

figure;
for i=2:length(t)       % Start at 2, initial conditions already defined.
    fprintf('==================TIME %f=====================\n',t(i))
    stresses(i,:) = stress(t(i));       % Desired stress, next step.
    strains(i,:) = strains(i-1,:);      % Initialize the next-step strain, will be incremented later.
    d_epsilon = d_strain(t(i-1))';      % Initial strain increment.

    % Newton iteration loop for stress controlled loading.
    for j=1:50
        % Update the stresses and strains.
        strains(i,:) = strains(i-1,:) + d_epsilon';
        sigma = stresses(i-1,:)' + matl.Ce*d_epsilon;
        [sigma,kappa_n1,lambda_n1,CTO] = integrator(matl,F,kappa,Q,sigma,kappa_n,lambda_n);
        % [sigma,kappa_n,lambda_n,CTO] = integrator_2invariant_m(matl,F,Q,kappa,kappa_n,lambda_n,sigma);

        r = stresses(i,:) - sigma';
        r(strain_ctrl_idxs) = 0.0;
        if(j==1) limit = norm(r); end
        convergence(j) = norm(r)/limit;
        fprintf('Outer Loop!  convergence: %g, j:%i\n',convergence(j),j);
        if(convergence(j) <= TOL) break; end

        invCTO = inv(CTO);
        %%%%% Define r(1) such that d_epsilon(1)=0.0
        %% For axisymmetric loading.
        % r(1) = -(invCTO(1,2:end)*r(2:end)') / invCTO(1,1);
        % d_epsilon = d_epsilon + invCTO*r';
        %% For plane strain loading.
        d_epsilon(3) = d_epsilon(3) + r(3)/CTO(3,3);

    end 
    % Update the stresses and internal variables, now that it's converged.
    stresses(i,:) = sigma';
    lambda_n = lambda_n1;
    kappa_n = kappa_n1;

    % Plot convergence rates for a couple of different times.
    if(t(i)==6)  semilogy(convergence,'ro-'); hold on; end
    if(t(i)==11) semilogy(convergence,'go-'); hold on; end
    if(t(i)==16) semilogy(convergence,'bo-'); hold on; end

    % Calculations for plotting later.
    q(i) = sqrt(3/2)*voigt_norm(sigma - sum(sigma(1:3))/3.0*I);
    epsilon_s(i) = sqrt(2/3)*voigt_norm(strains(i,:)'-sum(strains(i,1:3))/3*I);
    epsilon_v(i) = sum(strains(i,1:3));
end
hold off; title('Convergence Rate');xlabel('Iteration Step'); ylabel('||r||/||r_0||'); grid on;
legend('1.0%','2.0%','3.0%');

%===================== Post-Processing ======================%
stresses
strains
q
epsilon_s
epsilon_v
figure; plot(epsilon_s,q,'x-'); grid on;
title('q vs.\epsilon_s');xlabel('\epsilon_s');ylabel('q');
figure; plot(epsilon_s,epsilon_v,'x-'); grid on;
title('\epsilon_v vs. \epsilon_s');xlabel('\epsilon_s');ylabel('\epsilon_v');
figure; plot(strains(:,1),stresses(:,1),'x-'); grid on;
title('Stress vs. Strain');xlabel('\epsilon_1');ylabel('\sigma_1');
% print -f1 -depsc 'axisymmetric-convergence'
% print -f2 -depsc 'axisymmetric-q_es'
% print -f3 -depsc 'axisymmetric-ev_es'
% print -f4 -depsc 'axisymmetric-stress_strain'
print -f1 -depsc 'plane_strain-convergence'
print -f2 -depsc 'plane_strain-q_es'
print -f3 -depsc 'plane_strain-ev_es'
print -f4 -depsc 'plane_strain-stress_strain'
end

