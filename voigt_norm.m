%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Original Author: Philip Moseley
%%                  Northwestern University
%%                  Philip.Moseley@u.northwestern.edu
%%  This code is licensed under the AFL v3.0 as freely distributable
%%  open source software. The full text of the AFL and all licensing
%%  information must be included with the distribution of this code.
%%
%% Upstream contributions are encouraged!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate the L2 norm of a voigt tensor.
function n = voigt_norm(M)
    if(length(M) < 6)
        disp('voigt_norm only works on 6x1 or 1x6 vectors');
        exit;
    end
    n = sqrt(sum(M(1:3).^2 + 2*(M(4:end).^2)));
end

